<div class="node<?php print ($sticky) ? " sticky" : ""; ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php if ($page == 0): ?>
    <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>
  <div class="info"><?php print $submitted ?></div>
  <div class="content">
  <?php print $content ?>
  </div>
<br class='clear' />
<?php if ($links): ?>
  <div class="links"><?php print $links ?></div>
<?php endif; ?>
<?php if ($terms): ?>
  <div class="terms">( <?php print t("categories").":".$terms ?> )</div>
<?php endif; ?>
</div>
